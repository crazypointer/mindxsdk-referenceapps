diff --git a/ResultMerge.py b/ResultMerge.py
index 93ea8fc..e276401 100644
--- a/ResultMerge.py
+++ b/ResultMerge.py
@@ -10,6 +10,9 @@ import dota_utils as util
 import re
 import time
 import polyiou
+import copy
+import argparse
+import cv2
 
 ## the thresh for nms when merge image
 nms_thresh = 0.3
@@ -71,23 +74,16 @@ def py_cpu_nms(dets, thresh):
 
     return keep
 
-def nmsbynamedict(nameboxdict, nms, thresh):
+def nmsbynamedict(nameboxdict, nameboxdictclassname, nms, thresh): 
     nameboxnmsdict = {x: [] for x in nameboxdict}
     for imgname in nameboxdict:
-        #print('imgname:', imgname)
-        #keep = py_cpu_nms(np.array(nameboxdict[imgname]), thresh)
-        #print('type nameboxdict:', type(nameboxnmsdict))
-        #print('type imgname:', type(imgname))
-        #print('type nms:', type(nms))
         keep = nms(np.array(nameboxdict[imgname]), thresh)
-        #print('keep:', keep)
         outdets = []
-        #print('nameboxdict[imgname]: ', nameboxnmsdict[imgname])
         for index in keep:
-            # print('index:', index)
-            outdets.append(nameboxdict[imgname][index])
+            outdets.append(nameboxdictclassname[imgname][index])
         nameboxnmsdict[imgname] = outdets
     return nameboxnmsdict
+
 def poly2origpoly(poly, x, y, rate):
     origpoly = []
     for i in range(int(len(poly)/2)):
@@ -103,8 +99,11 @@ def mergebase(srcpath, dstpath, nms):
         name = util.custombasename(fullname)
         #print('name:', name)
         dstname = os.path.join(dstpath, name + '.txt')
+        if not os.path.exists(dstpath):
+            os.makedirs(dstpath)
         with open(fullname, 'r') as f_in:
             nameboxdict = {}
+            nameboxdict_classname = {} 
             lines = f_in.readlines()
             splitlines = [x.strip().split(' ') for x in lines]
             for splitline in splitlines:
@@ -122,24 +121,35 @@ def mergebase(srcpath, dstpath, nms):
                 rate = re.findall(pattern2, subname)[0]
 
                 confidence = splitline[1]
-                poly = list(map(float, splitline[2:]))
+                classname = splitline[-1]
+                poly = list(map(float, splitline[2:10]))
                 origpoly = poly2origpoly(poly, x, y, rate)
                 det = origpoly
                 det.append(confidence)
                 det = list(map(float, det))
+                det = list(map(float, det))
+                det_classname = copy.deepcopy(det)
+                det_classname.append(classname)
                 if (oriname not in nameboxdict):
                     nameboxdict[oriname] = []
+                    nameboxdict_classname[oriname] = []
                 nameboxdict[oriname].append(det)
-            nameboxnmsdict = nmsbynamedict(nameboxdict, nms, nms_thresh)
+                nameboxdict_classname[oriname].append(det_classname)
+
+            nameboxnmsdict = nmsbynamedict(nameboxdict, nameboxdict_classname, nms, nms_thresh)
             with open(dstname, 'w') as f_out:
                 for imgname in nameboxnmsdict:
                     for det in nameboxnmsdict[imgname]:
                         #print('det:', det)
-                        confidence = det[-1]
-                        bbox = det[0:-1]
-                        outline = imgname + ' ' + str(confidence) + ' ' + ' '.join(map(str, bbox))
+                        confidence = det[-2]
+                        bbox = det[0:-2]
+                        outline = imgname + ' ' + str(confidence) + ' ' + ' '.join(map(str, bbox)) + ' ' + det[-1] 
                         #print('outline:', outline)
                         f_out.write(outline + '\n')
+            f_out.close()
+            print(name + " merge down!") 
+        f_in.close()
+
 def mergebyrec(srcpath, dstpath):
     """
     srcpath: result files before merge and nms
@@ -162,7 +172,106 @@ def mergebypoly(srcpath, dstpath):
     mergebase(srcpath,
               dstpath,
               py_cpu_nms_poly)
+
+def draw(imgpath, imgsavepath, polys, confidences, class_names, labels):
+    """
+
+    :param imgpath: uncropped image source path
+    :param imgsavepath: the path to save the merged image
+    :param polys: the list containing object coordinate information
+    :param confidences: the list containing object coordinate information
+    :param class_names: the list containing object coordinate information
+    :param labels: a flag determining whether to print labels
+    """
+    random_seed = 666
+    class_num = 16
+    img_channel = 3
+    thickness_line = 1
+    thickness_font = thickness_line - 1 if (thickness_line - 1 > 1) else 1
+    classnames = ['plane', 'baseball-diamond', 'bridge', 'ground-track-field', 
+                  'small-vehicle', 'large-vehicle', 'ship', 'tennis-court',
+                  'basketball-court', 'storage-tank', 'soccer-ball-field', 'roundabout', 
+                  'harbor', 'swimming-pool', 'helicopter', 'container-crane']
+    np.random.seed(random_seed)
+    colors = [[np.random.randint(0, 255) for _ in range(img_channel)] for _ in range(class_num)] 
+
+    img = cv2.imread(imgpath)
+    for idx, poly in enumerate(polys):
+        classname = class_names[idx]
+        confidence = confidences[idx]
+        poly = np.array(poly, dtype=float).reshape(4, 2)
+        poly = np.int0(poly)
+        cv2.drawContours(image=img, contours=[poly], contourIdx=-1,
+                         color=colors[int(classnames.index(classname))],
+                         thickness=2)
+        if labels:
+            label = '%s %.2f' % (classname, confidence)
+        else:
+            label = '%s' % int(classnames.index(classname))
+        
+        text_size = cv2.getTextSize(label, 0, fontScale=thickness_line / 4, 
+                                    thickness=thickness_font)[0]
+        rec = cv2.minAreaRect(poly)
+        c1 = (int(rec[0][0]), int(rec[0][1]))
+        c2 = c1[0] + text_size[0], c1[1] - text_size[1] - 3
+        cv2.rectangle(img, c1, c2, colors[int(classnames.index(classname))], -1, cv2.LINE_AA) 
+        cv2.putText(img, label, (c1[0], c1[1] - 2), 0, thickness_line / 4, [225, 255, 255], 
+                    thickness=thickness_font, lineType=cv2.LINE_AA)
+    cv2.imwrite(imgsavepath, img)
+
+def draw_merged_img(srcimgpath, dstimgpath, mergedlabelpath, labels):
+    """Draw merged image.
+
+    :param srcimgpath: image source path
+    :param dstimgpath: the path to save the merged image
+    :param labels: a flag determining whether to print labels
+    """
+    if not os.path.exists(dstimgpath):
+        os.makedirs(dstimgpath)
+    if not os.path.exists(mergedlabelpath):
+        print("The merged labels folder does not exit!")
+        return
+    if len(os.listdir(mergedlabelpath)) == 0:
+        print("The merged labels folder is empty.") 
+        return
+    for root, _, files in os.walk(mergedlabelpath):
+        for filespath in files:
+            filepath = os.path.join(root, filespath)
+            with open(filepath, 'r') as f:
+                lines = f.readlines()
+                split_lines = []
+                polys = []
+                confidences = []
+                class_names = []
+                for line in lines:
+                    split_lines.append(line.strip().split(" "))
+                for split_line in split_lines:
+                    confidences.append(float(split_line[1]))
+                    polys.append(split_line[2:-1])
+                    class_names.append(split_line[-1])
+                name = split_lines[0][0]
+                imgpath = os.path.join(srcimgpath, name + '.jpg')
+                imgsavepath = os.path.join(dstimgpath, name + '_merged.jpg')
+                draw(imgpath, imgsavepath, polys, confidences, class_names, labels)
+                print(name + " draw down!")
+            f.close()
+
 if __name__ == '__main__':
+
+    parser = argparse.ArgumentParser() 
+    parser.add_argument('--labels_print', action='store_true', default=False, help='whether to print labels')   
+    parser.add_argument('--draw_img', action='store_true', default=False, help='whether to draw images') 
+    opt = parser.parse_args()
+    labels_print = opt.labels_print
+    draw_img = opt.draw_img
+
     # see demo for example
-    mergebypoly(r'path_to_configure', r'path_to_configure')
-    # mergebyrec()
\ No newline at end of file
+    mergebypoly(r'../detection_evaluation/result_txt/result_before_merge', 
+                r'../detection_evaluation/result_txt/result_merged')
+    # mergebyrec()
+    if draw_img: 
+        draw_merged_img(srcimgpath=r'../image', 
+                        dstimgpath=r'../detection/merged_drawed',
+                        mergedlabelpath=r'../detection/result_txt/result_merged',
+                        labels=labels_print
+                        )
\ No newline at end of file
diff --git a/SplitOnlyImage.py b/SplitOnlyImage.py
index 0bcf6ef..624fc29 100644
--- a/SplitOnlyImage.py
+++ b/SplitOnlyImage.py
@@ -8,9 +8,9 @@ class splitbase():
     def __init__(self,
                  srcpath,
                  dstpath,
-                 gap=100,
+                 gap=200,
                  subsize=1024,
-                 ext='.png'):
+                 ext='.jpg'):
         self.srcpath = srcpath
         self.outpath = dstpath
         self.gap = gap
@@ -19,15 +19,16 @@ class splitbase():
         self.srcpath = srcpath
         self.dstpath = dstpath
         self.ext = ext
-    def saveimagepatches(self, img, subimgname, left, up, ext='.png'):
+        if not os.path.exists(self.outpath):
+            os.makedirs(self.outpath)
+    def saveimagepatches(self, img, subimgname, left, up, ext='.jpg'): 
         subimg = copy.deepcopy(img[up: (up + self.subsize), left: (left + self.subsize)])
         outdir = os.path.join(self.dstpath, subimgname + ext)
         cv2.imwrite(outdir, subimg)
 
     def SplitSingle(self, name, rate, extent):
-        img = cv2.imread(os.path.join(self.srcpath, name + extent))
+        img = cv2.imread(os.path.join(self.srcpath, name + extent))       
         assert np.shape(img) != ()
-
         if (rate != 1):
             resizeimg = cv2.resize(img, None, fx=rate, fy=rate, interpolation = cv2.INTER_CUBIC)
         else:
@@ -57,12 +58,25 @@ class splitbase():
                 left = left + self.slide
 
     def splitdata(self, rate):
-        
+        if not os.path.exists(self.srcpath):
+            print("The path of image folder to crop does not exist!")
+            return
         imagelist = util.GetFileFromThisRootDir(self.srcpath)
+        if len(imagelist) == 0:
+            print("Image folder is empty!")
+            return
+        for image_path in imagelist:
+            _, suffix = os.path.splitext(image_path)
+            if suffix != '.jpg':
+                print("The input file(path: {}) is not a image with jpg format!".format(image_path))
+                imagelist.remove(image_path)
+        if len(imagelist) == 0:
+            return
         imagenames = [util.custombasename(x) for x in imagelist if (util.custombasename(x) != 'Thumbs')]
         for name in imagenames:
             self.SplitSingle(name, rate, self.ext)
+            print(name, "split down!")
 if __name__ == '__main__':
-    split = splitbase(r'example/images',
-                      r'example/imagesSplit')
+    split = splitbase(r'../image',
+                  r'../imageSplit')
     split.splitdata(1)
\ No newline at end of file
diff --git a/dota-v1.5_evaluation_task1.py b/dota-v1.5_evaluation_task1.py
index 23e3267..34258a5 100644
--- a/dota-v1.5_evaluation_task1.py
+++ b/dota-v1.5_evaluation_task1.py
@@ -10,14 +10,60 @@
     search for PATH_TO_BE_CONFIGURED to config the paths
     Note, the evaluation is on the large scale images
 """
-import xml.etree.ElementTree as ET
 import os
-#import cPickle
 import numpy as np
-import matplotlib.pyplot as plt
 import polyiou
-from functools import partial
-import pdb
+import shutil
+
+def check_directoy(dstpath):
+    if os.path.exists(dstpath):
+        for root, _, files in os.walk(dstpath):
+            for file in files:
+                os.remove(os.path.join(root, file))
+        shutil.rmtree(dstpath)
+    os.makedirs(dstpath)
+
+def generate_imgnamefile(srcpath, dstpath):
+    """
+
+    :param srcpath: dataset image path
+    :param dstpath: the path to save imgnamefile
+    """
+    check_directoy(dstpath)
+    dstname = os.path.join(dstpath, 'imgnamefile.txt')
+    if os.path.exists(dstname):
+        os.remove(dstname)
+    for _, _, files in os.walk(srcpath):
+        for file in files:
+            name = os.path.splitext(file)[0]
+            with open(dstname, 'a') as f:
+                f.writelines(name + '\n')
+            f.close()
+
+def generate_classnamefile(srcpath, dstpath):
+    """
+
+    :param srcpath: the path containing the results after merged
+    :param dstpath: the path to save classnamefile
+    """
+    check_directoy(dstpath)
+    filepaths = []
+    for root, _, files in os.walk(srcpath):
+        for file in files:
+            filepath = os.path.join(root, file)
+            filepaths.append(filepath)
+    for filepath in filepaths:
+        with open(filepath, 'r') as f:
+            lines = f.readlines()
+            splitlines = [line.strip().split(' ') for line in lines]
+        f.close()
+        for splitline in splitlines:
+            classname = splitline[-1]
+            dstname = os.path.join(dstpath, 'Task1_' + classname + '.txt')
+            object = ' '.join(splitline[0:-1])
+            with open(dstname, 'a') as f:
+                f.writelines(object + '\n')
+            f.close()
 
 def parse_gt(filename):
     """
@@ -139,7 +185,7 @@ def voc_eval(detpath,
     for imagename in imagenames:
         R = [obj for obj in recs[imagename] if obj['name'] == classname]
         bbox = np.array([x['bbox'] for x in R])
-        difficult = np.array([x['difficult'] for x in R]).astype(np.bool)
+        difficult = np.array([x['difficult'] for x in R]).astype(bool)
         det = [False] * len(R)
         npos = npos + sum(~difficult)
         class_recs[imagename] = {'bbox': bbox,
@@ -243,11 +289,7 @@ def voc_eval(detpath,
 
     # compute precision recall
 
-    print('check fp:', fp)
-    print('check tp', tp)
-
-
-    print('npos num:', npos)
+    # print('npos num:', npos)
     fp = np.cumsum(fp)
     tp = np.cumsum(tp)
 
@@ -261,9 +303,19 @@ def voc_eval(detpath,
 
 def main():
 
-    detpath = r'/home/dingjian/data/DOTA-v1.5/example/RoITrans/Task1_{:s}.txt'
-    annopath = r'/home/dingjian/code/DOAI_server2/media/DOTA15_Task1_gt/{:s}.txt'
-    imagesetfile = r'/home/dingjian/code/DOAI_server2/media/testset.txt'
+    detection_output_path = r'../detection_evaluation'
+    dataset_path = r'../dataset'
+    img_path = str(dataset_path + '/images')
+    anno_path = str(dataset_path + '/labelTxt')
+    imgfilename_path = str(detection_output_path + '/result_txt/image_name')
+    classname_path = str(detection_output_path + '/result_txt/result_classname')
+    result_merge_path =  str(detection_output_path + '/result_txt/result_merged')
+    generate_imgnamefile(img_path, imgfilename_path)
+    generate_classnamefile(result_merge_path, classname_path)
+
+    detpath = str(detection_output_path + '/result_txt/result_classname/Task1_{:s}.txt')
+    annopath = str(anno_path + '/{:s}.txt')
+    imagesetfile = str(imgfilename_path + '/imgnamefile.txt')
 
     # detpath = r'PATH_TO_BE_CONFIGURED/Task1_{:s}.txt'
     # annopath = r'PATH_TO_BE_CONFIGURED/{:s}.txt' # change the directory to the path of val/labelTxt, if you want to do evaluation on the valset
@@ -277,9 +329,15 @@ def main():
     #             'basketball-court', 'storage-tank',  'soccer-ball-field', 'roundabout', 'harbor', 'swimming-pool', 'helicopter', ']
     classaps = []
     map = 0
+    skipclass_cont = 0 
     for classname in classnames:
         print('classname:', classname)
-        rec, prec, ap = voc_eval(detpath,
+        detfile = detpath.format(classname)
+        if not (os.path.exists(detfile)): 
+            skipclass_cont += 1
+            print('This class is not be detected in your dataset: {}'.format(classname))
+            continue
+        rec, prec, ap = voc_eval(detfile, 
              annopath,
              imagesetfile,
              classname,
@@ -296,9 +354,10 @@ def main():
         # plt.ylabel('precision')
         # plt.plot(rec, prec)
        # plt.show()
-    map = map/len(classnames)
+    map = map/(len(classnames)-skipclass_cont)
     print('map:', map)
     classaps = 100*np.array(classaps)
     print('classaps: ', classaps)
 if __name__ == '__main__':
-    main()
\ No newline at end of file
+    main()
+    
\ No newline at end of file
